//
//  Github+Moya.swift
//  AllStarsJava
//
//  Created by Luiz Fernando Silva on 02/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import UIKit
import Moya

enum Github {
    case javaRepos(page: Int)
    case pullRequests(author: String, repo: String, page: Int)
}

extension Github: TargetType {
    var baseURL: URL {
        return URL(string: "https://api.github.com")!
    }
    
    var path: String {
        switch self {
        case .javaRepos:
            return "/search/repositories"
        case let .pullRequests(author, repo, _):
            return "/repos/\(author)/\(repo)/pulls"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .javaRepos, .pullRequests:
            return .get
        }
    }
    
    var task: Task {
        switch self {
        case .javaRepos(let page):
            return .requestParameters(parameters: ["page": page, "q": "language:Java", "sort": "stars"],
                                      encoding: URLEncoding.queryString)
        case .pullRequests(_, _, let page):
            return .requestParameters(parameters: ["page": page],
                                      encoding: URLEncoding.queryString)
        }
    }
    
    var sampleData: Data {
        switch self {
        case .javaRepos:
            return """
                {

                    "total_count": 1,
                    "incomplete_results": false,
                    "items": [
                        {
                            "id": 7508411,
                            "name": "RxJava",
                            "owner": {
                                "login": "ReactiveX",
                                "avatar_url": "https://avatars1.githubusercontent.com/u/6407041?v=4"
                            },
                            "description": "RxJava – Reactive Extensions for the JVM – a library for composing asynchronous and event-based programs using observable sequences for the Java VM.",
                            "stargazers_count": 28364,
                            "forks_count": 5001
                            }
                       ]

                }
                """.data(using: .utf8)!
            
        case .pullRequests:
            return """
                [
                    {
                        "id": 129571973,
                        "html_url": "https://github.com/elastic/elasticsearch/pull/27237",
                        "title": "Uses norms for exists query if enabled",
                        "user": {
                            "login": "colings86",
                            "avatar_url": "https://avatars0.githubusercontent.com/u/236731?v=4"
                        },
                        "body": "This change means that for indexes created from 6.1.0, if normas are enabled we will not write the field name to the `_field_names` field and for an exists query we will instead use the NormsFieldExistsQuery which was added in Lucene 7.1.0. If norms are not enabled or if the index was created before 6.1.0 `_field_names` will be used as before.",
                        "created_at": "2017-11-02T16:01:49Z",
                        "updated_at": "2017-11-02T15:55:06Z"
                    }
                ]
                """.data(using: .utf8)!
        }
    }
    
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}
