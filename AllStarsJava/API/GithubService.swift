//
//  GithubService.swift
//  AllStarsJava
//
//  Created by Luiz Fernando Silva on 02/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case error(Error)
}

protocol GithubService {
    func repositories(page: Int, callback: @escaping (Result<RepositorySearchResult>) -> ())
    func pullRequests(repo: Repository, page: Int, callback: @escaping (Result<[PullRequest]>) -> ())
}
