//
//  GithubServiceImpl.swift
//  AllStarsJava
//
//  Created by Luiz Fernando Silva on 02/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import Foundation
import Moya

class GithubServiceImpl: GithubService {
    private lazy var iso8601Formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"
        
        return formatter
    }()
    
    var provider = MoyaProvider<Github>()
    
    func repositories(page: Int, callback: @escaping (Result<RepositorySearchResult>) -> ()) {
        provider.request(.javaRepos(page: page)) { moyaResult in
            let result: Result<RepositorySearchResult>
            
            do {
                switch moyaResult {
                case .success(let response):
                    let decoder = self.jsonDecoder()
                    
                    result = .success(try decoder.decode(RepositorySearchResult.self, from: response.data))
                    
                case .failure(let error):
                    throw error
                }
            } catch {
                result = .error(error)
            }
            
            DispatchQueue.main.async {
                callback(result)
            }
        }
    }
    
    func pullRequests(repo: Repository, page: Int, callback: @escaping (Result<[PullRequest]>) -> ()) {
        provider.request(.pullRequests(author: repo.author.name, repo: repo.name, page: page)) { moyaResult in
            let result: Result<[PullRequest]>
            
            do {
                switch moyaResult {
                case .success(let response):
                    let decoder = self.jsonDecoder()
                    
                    result = .success(try decoder.decode([PullRequest].self, from: response.data))
                    
                case .failure(let error):
                    throw error
                }
            } catch {
                result = .error(error)
            }
            
            DispatchQueue.main.async {
                callback(result)
            }
        }
    }
    
    private func jsonDecoder() -> JSONDecoder {
        let decoder = JSONDecoder()
        if #available(iOS 10.0, *) {
            decoder.dateDecodingStrategy = .iso8601
        } else {
            decoder.dateDecodingStrategy = .formatted(iso8601Formatter)
        }
        
        return decoder
    }
}
