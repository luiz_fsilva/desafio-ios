//
//  AppDelegate.swift
//  AllStarsJava
//
//  Created by Luiz Fernando Silva on 30/10/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}
