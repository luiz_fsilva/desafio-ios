//
//  AuthorAvatarDataSource+SDWebImage.swift
//  AllStarsJava
//
//  Created by Luiz Fernando Silva on 03/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import UIKit
import SDWebImage

class AuthorAvatarDataSourceImpl: AuthorAvatarDataSource {
    func loadAuthorAvatar(_ author: Author, to imageView: UIImageView, placeholder: UIImage?) -> Cancellable? {
        if let placeholder = placeholder {
            imageView.image = placeholder
        }
        
        guard let url = URL(string: author.avatarUrl) else {
            return nil
        }
        
        let operation =
            SDWebImageManager
                .shared()
                .loadImage(with: url, options: [], progress: nil) { (image, aa, ab, ac, ad, ae) in
                    DispatchQueue.main.async {
                        if let image = image {
                            imageView.image = image
                        }
                    }
                }
        
        if let operation = operation {
            return _SDWebImageOperationBridge(operation: operation)
        }
        
        return nil
    }
    
    /// Simple bridge wrapper for `SDWebImageOperation` -> `Cancellable`
    private class _SDWebImageOperationBridge: Cancellable {
        var operation: SDWebImageOperation
        
        init(operation: SDWebImageOperation) {
            self.operation = operation
        }
        
        func cancel() {
            operation.cancel()
        }
    }
}
