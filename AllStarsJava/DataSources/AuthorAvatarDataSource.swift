//
//  AuthorAvatarDataSource.swift
//  AllStarsJava
//
//  Created by Luiz Fernando Silva on 03/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import UIKit

/// Defines a generic cancellable operation
protocol Cancellable {
    func cancel()
}

/// Handles asynchronous downloading of author avatar images
protocol AuthorAvatarDataSource {
    /// Downloads an author's avatar image from a given URL.
    /// A placeholder is used while the image is not fully downloaded.
    /// If the URL is not valid, the placeholder image provided is used instead.
    /// If no placeholder image is provided, the image view's `.image` property
    /// is not modified.
    ///
    /// - Parameters:
    ///   - author: Author to download image from. If author's avatar string not
    /// a valid URL string, placeholder image is used instead.
    ///   - imageView: Image view to replace the image of when download finishes.
    ///   - placeholder: A placeholder image to use during image downloading.
    /// If nil, and author's avatar url is not a valid URL string, image view is
    /// not modified.
    /// - Returns: A cancellable operation to allow early cancelling of any async
    /// download operation. If `nil`, no cancellable operation was performed.
    func loadAuthorAvatar(_ author: Author, to imageView: UIImageView,
                          placeholder: UIImage?) -> Cancellable?
}

