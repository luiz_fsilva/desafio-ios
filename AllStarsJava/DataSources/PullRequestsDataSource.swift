//
//  PullRequestsDataSource.swift
//  AllStarsJava
//
//  Created by Luiz Fernando Silva on 03/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import UIKit

class PullRequestsDataSource: NSObject, UITableViewDataSource {
    
    var markdown = CachingMarkdownImpl()
    
    var pullRequests: [PullRequest]
    var tableView: UITableView
    
    init(pullRequests: [PullRequest], tableView: UITableView) {
        self.pullRequests = pullRequests
        self.tableView = tableView
        
        super.init()
        
        tableView.dataSource = self
        
        tableView.registerCellNib(type: PullRequestTableViewCell.self)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 113
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullRequests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PullRequestTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        
        cell.markdown = markdown
        
        cell.configure(with: pullRequests[indexPath.row])
        
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
}

class CachingMarkdownImpl: MarkdownImpl {
    private var cache: [Int: NSAttributedString] = [:]
    
    override func markdownBody(for pullRequest: PullRequest) -> NSAttributedString {
        if let cached = cache[pullRequest.id] {
            return cached
        }
        
        let string = super.markdownBody(for: pullRequest)
        cache[pullRequest.id] = string
        
        return string
    }
}
