//
//  RepositoriesDataSource.swift
//  AllStarsJava
//
//  Created by Luiz Fernando Silva on 02/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import UIKit

class RepositoriesDataSource: NSObject, UITableViewDataSource {
    
    var tableView: UITableView
    var repositories: [Repository]
    
    init(repositories: [Repository], tableView: UITableView) {
        self.repositories = repositories
        self.tableView = tableView
        
        tableView.registerCellNib(type: RepositoryTableViewCell.self)
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 72
        
        super.init()
        
        tableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let repo = repositories[indexPath.row]
        
        let cell: RepositoryTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        
        cell.accessoryType = .disclosureIndicator
        cell.configure(with: repo)
        
        return cell
    }
}
