//
//  Markdown.swift
//  AllStarsJava
//
//  Created by Luiz Fernando Silva on 03/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import UIKit
import TSMarkdownParser

protocol Markdown {
    func markdownBody(for pullRequest: PullRequest) -> NSAttributedString
}

class MarkdownImpl: Markdown {
    
    public var markdownParser: TSMarkdownParser
    
    init() {
        markdownParser = TSMarkdownParser.standard()
        
        setupMarkDownConfig()
    }
    
    func markdownBody(for pullRequest: PullRequest) -> NSAttributedString {
        if let body = pullRequest.body {
            return markdownParser.attributedString(fromMarkdown: body)
        }
        
        return NSAttributedString(string: "")
    }
    
    private func setupMarkDownConfig() {
        guard let defaultFont = markdownParser.defaultAttributes?[NSAttributedStringKey.font.rawValue] as? UIFont else {
            return
        }
        
        guard let courier = UIFont(name: "Menlo-regular", size: defaultFont.pointSize) ?? UIFont(name: "Courier New", size: defaultFont.pointSize) else {
            return
        }
        
        markdownParser.monospaceAttributes = [
            NSAttributedStringKey.font.rawValue: courier,
            NSAttributedStringKey.backgroundColor.rawValue: UIColor(white: 0.94, alpha: 1),
            NSAttributedStringKey.foregroundColor.rawValue: UIColor(white: 0.15, alpha: 1)
        ]
    }
}
