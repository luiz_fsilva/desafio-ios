//
//  PullRequest.swift
//  AllStarsJava
//
//  Created by Luiz Fernando Silva on 02/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import Foundation

// The keys and values from these models match the model described on the
// following doc: https://developer.github.com/v3/pulls/#list-pull-requests

struct PullRequest: Codable {
    var id: Int
    var url: String
    var title: String
    var user: Author
    var body: String?
    var createdAt: Date
    var updatedAt: Date
    
    enum CodingKeys: String, CodingKey {
        case id
        case url = "html_url"
        case title
        case user
        case body
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}
