//
//  Repository.swift
//  AllStarsJava
//
//  Created by Luiz Fernando Silva on 02/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import Foundation

// The keys and values from these models match the model described on the
// following doc: https://developer.github.com/v3/search/#search-repositories

struct Repository: Codable {
    var name: String
    var description: String?
    var author: Author
    var starsCount: Int
    var forksCount: Int
    
    enum CodingKeys: String, CodingKey {
        case name
        case description
        case author = "owner"
        case starsCount = "stargazers_count"
        case forksCount = "forks_count"
    }
}
