//
//  RepositorySearchResponse.swift
//  AllStarsJava
//
//  Created by Luiz Fernando Silva on 02/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import Foundation

// The keys and values from these models match the model described on the
// following doc: https://developer.github.com/v3/search/#search-repositories

struct RepositorySearchResult: Codable {
    var totalCount: Int
    var items: [Repository]
    
    enum CodingKeys: String, CodingKey {
        case totalCount = "total_count"
        case items
    }
}
