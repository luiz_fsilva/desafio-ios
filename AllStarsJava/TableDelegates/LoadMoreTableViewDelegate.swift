//
//  LoadMoreTableViewDelegate.swift
//  AllStarsJava
//
//  Created by Luiz Fernando Silva on 02/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import UIKit

/// Adds bottom 'push-to-load-more'/'infinite load'-style functionality to table
/// views
class LoadMoreTableViewDelegate: NSObject, UITableViewDelegate {
    
    var tableView: UITableView
    var loadMoreCallback: () -> ()
    var distance: CGFloat = -30
    
    // Se fix-me bellow
    var didSelectCell: ((IndexPath) -> ())?
    
    init(tableView: UITableView, callback: @escaping () -> ()) {
        self.tableView = tableView
        self.loadMoreCallback = callback
        
        super.init()
        
        tableView.delegate = self
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let currentOffset = targetContentOffset.pointee.y
        let maximumOffset = scrollView.contentSize.height - scrollView.bounds.height
        
        if currentOffset - maximumOffset >= distance {
            loadMoreCallback()
        }
    }
    
    // FIXME: Hacky! This is not this delegate's responsibility, but we need to
    // handle cell selection somewhere. Probably the best way to do that would be
    // create a UITableViewDelegate class and inject this delegate within it,
    // forwarding calls to `scrollViewWillEndDragging(_:withVelocity:targetContentOffset:)`
    // to it.
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        didSelectCell?(indexPath)
    }
}
