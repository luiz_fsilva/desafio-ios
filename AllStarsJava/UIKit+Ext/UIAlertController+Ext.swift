//
//  UIAlertController+Ext.swift
//  AllStarsJava
//
//  Created by Luiz Fernando Silva on 03/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import UIKit

extension UIAlertController {
    /// Presents a simple alert w/ a single 'Ok' button.
    public static func presentAlert(withTitle title: String, message: String,
                                    on viewController: UIViewController)
    {
        let controller = UIAlertController(title: title, message: message,
                                           preferredStyle: .alert)
        
        controller.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        viewController.present(controller, animated: true)
    }
}
