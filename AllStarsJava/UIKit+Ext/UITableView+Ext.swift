//
//  UITableView+Ext.swift
//  AllStarsJava
//
//  Created by Luiz Fernando Silva on 02/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import UIKit

// Contains some helpers to deal with table view cell registration/dequeing
// without having to deal with string consts everywhere.

extension UITableView {
    func registerCellNib(type: UITableViewCell.Type) {
        register(type.nibForCell(), forCellReuseIdentifier: type.reuseIdentifier)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(type cellType: T.Type = T.self, for indexPath: IndexPath) -> T {
        let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath)
        
        guard let casted = cell as? T else {
            fatalError("Failed to cast cell of type \(type(of: cell)) to expected type \(T.self)!")
        }
        
        return casted
    }
}

extension UITableViewCell {
    /// Reuse identifier.
    /// Defaults to dynamic name of `self`.
    static var reuseIdentifier: String {
        return "\(self)"
    }
    
    /// Returns the dynamic name of `self`
    static var nibName: String {
        return "\(self)"
    }
    
    static func nibForCell() -> UINib {
        let bundle = Bundle(for: self)
        
        return UINib(nibName: nibName, bundle: bundle)
    }
    
    /// Tries to instantiate an instance an instance of a table view cell from a
    /// known nib name.
    /// Method returns `nil` if initialization failed.
    static func instantiate<T: UITableViewCell>(fromNibNamed nibName: String = nibName) -> T? {
        let cell = T()
        
        let nib = nibForCell()
        let nibView = nib.instantiate(withOwner: cell, options: nil).first
        
        return nibView as? T
    }
}
