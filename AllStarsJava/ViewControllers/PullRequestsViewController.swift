//
//  PullRequestsViewController.swift
//  AllStarsJava
//
//  Created by Luiz Fernando Silva on 03/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import UIKit

class PullRequestsViewController: UIViewController {
    
    /// Repository to display pull requests from
    var repository: Repository?
    
    private var loadedInitialData = false
    private var page = 1
    private var isLoading = false
    private var hasMoreData = true
    
    @IBOutlet weak var tableView: UITableView!
    
    var service: GithubService = GithubServiceImpl()
    
    var loadMoreDelegate: LoadMoreTableViewDelegate!
    var dataSource: PullRequestsDataSource!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTitle()
        setupDataSource()
        setupLoadMoreDelegate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !loadedInitialData {
            loadedInitialData = true
            loadMoreData(showTableSpinner: false)
        }
    }
    
    func loadMoreData(showTableSpinner: Bool) {
        guard let repository = repository else {
            return
        }
        
        if isLoading || !hasMoreData {
            return
        }
        
        isLoading = true
        
        if showTableSpinner {
            tableView.tableFooterView = { () -> UILabel in
                let label = UILabel()
                label.text = "Carregando..."
                label.textAlignment = .center
                label.sizeToFit()
                label.frame.size.height += 15
                
                return label
            }()
        } else {
            LoadingHud.showHud(label: "Carregando...", in: view)
        }
        
        service.pullRequests(repo: repository, page: page) { result in
            LoadingHud.dismissHud(in: self.view)
            
            switch result {
            case .success(let results):
                
                // Append to current data
                self.dataSource.pullRequests.append(contentsOf: results)
                
                self.tableView.reloadData()
                
                DispatchQueue.main.async {
                    self.tableView.tableFooterView = nil
                }
                
                self.page += 1
                
                // If no more results have been served, assume we reached the end
                // of available data
                if results.count == 0 {
                    self.hasMoreData = false
                }
            case .error:
                UIAlertController
                    .presentAlert(withTitle: "Ops!",
                                  message: """
                                    Um erro inesperado ocorreu ao tentar buscar \
                                    as informações de pull requests!
                                    """, on: self)
            }
            
            self.isLoading = false
        }
    }
    
    func browseToPullRequest(at index: Int) {
        let pull = dataSource.pullRequests[index]
        
        guard let url = URL(string: pull.url) else {
            return
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    func setupTitle() {
        title = repository?.name
    }

    func setupDataSource() {
        dataSource = PullRequestsDataSource(pullRequests: [], tableView: tableView)
        
        tableView.reloadData()
    }
    
    func setupLoadMoreDelegate() {
        loadMoreDelegate = LoadMoreTableViewDelegate(tableView: tableView) { [weak self] in
            self?.loadMoreData(showTableSpinner: true)
        }
        
        loadMoreDelegate.didSelectCell = { [weak self] indexPath in
            self?.browseToPullRequest(at: indexPath.row)
        }
    }
}
