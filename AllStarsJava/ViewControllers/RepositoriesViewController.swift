//
//  RepositoriesViewController.swift
//  AllStarsJava
//
//  Created by Luiz Fernando Silva on 30/10/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import UIKit

class RepositoriesViewController: UIViewController {
    
    let pullRequestsSegue = "pullRequests"
    
    // For segue
    private var selectedRepository: Repository?
    private var loadedInitialData = false
    private var page = 1
    private var isLoading = false
    
    @IBOutlet weak var tableView: UITableView!
    
    var service: GithubService = GithubServiceImpl()
    
    var loadMoreDelegate: LoadMoreTableViewDelegate!
    var dataSource: RepositoriesDataSource!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupDataSource()
        setupLoadMoreDelegate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !loadedInitialData {
            loadedInitialData = true
            loadMoreData(showTableSpinner: false)
        }
    }
    
    func loadMoreData(showTableSpinner: Bool) {
        if isLoading {
            return
        }
        
        isLoading = true
        
        if showTableSpinner {
            tableView.tableFooterView = { () -> UILabel in
                let label = UILabel()
                label.text = "Carregando..."
                label.textAlignment = .center
                label.sizeToFit()
                label.frame.size.height += 15
                
                return label
            }()
        } else {
            LoadingHud.showHud(label: "Carregando...", in: view)
        }
        
        service.repositories(page: page) { result in
            LoadingHud.dismissHud(in: self.view)
            
            switch result {
            case .success(let results):
                
                // Append to current data
                self.dataSource.repositories.append(contentsOf: results.items)
                
                self.tableView.reloadData()
                
                DispatchQueue.main.async {
                    self.tableView.tableFooterView = nil
                }
            
                self.page += 1
            case .error:
                UIAlertController
                    .presentAlert(withTitle: "Ops!",
                                  message: """
                                    Um erro inesperado ocorreu ao tentar buscar \
                                    as informações de repositórios!
                                    """, on: self)
            }
            
            self.isLoading = false
        }
    }
    
    func handleSelectRepository(_ index: Int) {
        selectedRepository = dataSource.repositories[index]
        performSegue(withIdentifier: pullRequestsSegue, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == pullRequestsSegue, let dest = segue.destination as? PullRequestsViewController {
            dest.repository = selectedRepository
        }
    }
    
    func setupDataSource() {
        dataSource = RepositoriesDataSource(repositories: [], tableView: tableView)
        
        tableView.reloadData()
    }
    
    func setupLoadMoreDelegate() {
        loadMoreDelegate = LoadMoreTableViewDelegate(tableView: tableView) { [weak self] in
            self?.loadMoreData(showTableSpinner: true)
        }
        
        loadMoreDelegate.didSelectCell = { [weak self] indexPath in
            self?.handleSelectRepository(indexPath.row)
        }
    }
}
