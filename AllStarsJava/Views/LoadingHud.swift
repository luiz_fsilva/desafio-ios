//
//  LoadingHud.swift
//  AllStarsJava
//
//  Created by Luiz Fernando Silva on 02/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import UIKit
import JGProgressHUD

enum LoadingHud {
    
    public static func showHud(label: String, in view: UIView) {
        let hud = JGProgressHUD(style: .dark)
        
        hud.textLabel.text = label
        
        hud.show(in: view)
    }
    
    public static func dismissHud(in view: UIView) {
        for hud in JGProgressHUD.allProgressHUDs(in: view) {
            hud.dismiss()
        }
    }
}
