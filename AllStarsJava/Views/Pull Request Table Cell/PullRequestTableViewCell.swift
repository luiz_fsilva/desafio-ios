//
//  PullRequestTableViewCell.swift
//  AllStarsJava
//
//  Created by Luiz Fernando Silva on 03/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {
    
    private var previousImageLoad: Cancellable?
    
    lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        
        formatter.locale = Locale.current
        formatter.doesRelativeDateFormatting = true
        
        return formatter
    }()
    
    var markdown: Markdown = MarkdownImpl()
    var authorAvatarDataSource: AuthorAvatarDataSource = AuthorAvatarDataSourceImpl()
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var imgUserAvatar: UIImageView!
    @IBOutlet weak var lblAuthorName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var lblBody: UILabel!
    
    func configure(with pullRequest: PullRequest) {
        
        lblTitle.text = pullRequest.title
        lblAuthorName.text = pullRequest.user.name
        lblDate.text = dateFormatter.string(from: pullRequest.createdAt)
        
        lblBody.attributedText = markdown.markdownBody(for: pullRequest)
        
        let defaultImg = UIImage(named: "person")
        
        previousImageLoad?.cancel()
        previousImageLoad =
            authorAvatarDataSource
                .loadAuthorAvatar(pullRequest.user, to: imgUserAvatar, placeholder: defaultImg)
    }
}
