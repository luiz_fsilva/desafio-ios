//
//  RepositoryTableViewCell.swift
//  AllStarsJava
//
//  Created by Luiz Fernando Silva on 30/10/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import UIKit
import SDWebImage

private var digitsFormatter: NumberFormatter = {
    let formatter = NumberFormatter()
    
    formatter.numberStyle = .decimal
    formatter.locale = Locale.current
    formatter.usesGroupingSeparator = true
    formatter.groupingSeparator = ","
    
    return formatter
}()

class RepositoryTableViewCell: UITableViewCell {

    private var previousImageLoad: Cancellable?
    
    var authorAvatarDataSource: AuthorAvatarDataSource = AuthorAvatarDataSourceImpl()
    
    @IBOutlet weak var imgUserAvatar: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblStarCount: UILabel!
    @IBOutlet weak var lblForkCount: UILabel!
    
    func configure(with repository: Repository) {
        let name = NSMutableAttributedString()
        name.append(NSAttributedString(string: repository.author.name,
                                       attributes: [NSAttributedStringKey.foregroundColor : UIColor.darkGray]))
        
        name.append(NSAttributedString(string: "/\(repository.name)",
                                       attributes: [NSAttributedStringKey.foregroundColor : UIColor.black]))
        
        lblName.attributedText = name
        lblDescription.text = repository.description
        
        lblStarCount.text = digitsFormatter.string(from: NSNumber(value: repository.starsCount))
        lblForkCount.text = digitsFormatter.string(from: NSNumber(value: repository.forksCount))
        
        let defaultImg = UIImage(named: "person")
        
        previousImageLoad?.cancel()
        previousImageLoad =
            authorAvatarDataSource
                .loadAuthorAvatar(repository.author, to: imgUserAvatar, placeholder: defaultImg)
    }
}
