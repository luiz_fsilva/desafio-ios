//
//  GithubMoyaTests.swift
//  AllStarsJavaTests
//
//  Created by Luiz Fernando Silva on 02/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import XCTest
import Moya
@testable import AllStarsJava

class GithubMoyaTests: XCTestCase {
    
    func testJavaReposPathAndMethod() {
        let request = Github.javaRepos(page: 1)
        
        XCTAssertEqual(request.path, "/search/repositories")
        XCTAssertEqual(request.method, .get)
    }
    
    func testPullRequestsPathAndMethod() {
        let request = Github.pullRequests(author: "test", repo: "repo", page: 1)
        
        XCTAssertEqual(request.path, "/repos/test/repo/pulls")
        XCTAssertEqual(request.method, .get)
    }
    
    func testJavaReposQuery() {
        switch Github.javaRepos(page: 3).task {
        case let .requestParameters(params, encoding):
            XCTAssertEqual(params["page"] as? Int, 3)
            XCTAssertEqual(params["q"] as? String, "language:Java")
            XCTAssertEqual(params["sort"] as? String, "stars")
            XCTAssert(encoding is URLEncoding)
        default:
            XCTFail("Expected 'page=?' request parameters!")
        }
    }
    
    func testPullRequestsQuery() {
        switch Github.pullRequests(author: "", repo: "", page: 3).task {
        case let .requestParameters(params, encoding):
            XCTAssertEqual(params["page"] as? Int, 3)
            XCTAssert(encoding is URLEncoding)
        default:
            XCTFail("Expected 'page=?' request parameters!")
        }
    }
}
