//
//  GithubServiceImplTests.swift
//  AllStarsJavaTests
//
//  Created by Luiz Fernando Silva on 03/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import XCTest
import Moya
@testable import AllStarsJava

class GithubServiceImplTests: XCTestCase {
    
    var mock: MoyaProvider<Github>!
    
    var sut: GithubServiceImpl!
    
    override func setUp() {
        super.setUp()
        
        mock = MoyaProvider<Github>(stubClosure: MoyaProvider.immediatelyStub)
        
        sut = GithubServiceImpl()
        sut.provider = mock
    }
    
    func testRepositoriesSuccess() throws {
        let exp = expectation(description: "test")
        
        sut.repositories(page: 1) { result in
            switch result {
            case .success(let repos):
                XCTAssertEqual(repos.totalCount, 1)
                XCTAssertEqual(repos.items.count, 1)
            case .error(let e):
                XCTFail("\(e)")
            }
            
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 0.5, handler: nil)
    }
    
    func testPullRequestsSuccess() throws {
        let exp = expectation(description: "test")
        
        let repo = Repository(name: "Repo 1", description: "Test",
                              author: Author(name: "Owner", avatarUrl: ""),
                              starsCount: 3, forksCount: 4000)
        
        sut.pullRequests(repo: repo, page: 1) { result in
            switch result {
            case .success(let pulls):
                XCTAssertEqual(pulls.count, 1)
            case .error(let e):
                XCTFail("\(e)")
            }
            
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 0.5, handler: nil)
    }
}
