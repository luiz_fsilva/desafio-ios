//
//  PullRequestsDataSourceTests.swift
//  AllStarsJavaTests
//
//  Created by Luiz Fernando Silva on 03/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import XCTest
@testable import AllStarsJava

class PullRequestsDataSourceTests: XCTestCase {
    
    var pullRequests: [PullRequest] = []
    var tableView: UITableView!
    
    var sut: PullRequestsDataSource!
    
    override func setUp() {
        super.setUp()
        
        pullRequests = [
            PullRequest(id: 1, url: "https://github.com/a/b/pulls/1", title: "Pull request 1",
                        user: Author(name: "User 1", avatarUrl: ""), body: "Abcdef",
                        createdAt: Date(), updatedAt: Date()),
            
            PullRequest(id: 2, url: "http://github.com/a/b/pulls/2", title: "Pull request 2",
                        user: Author(name: "User 2", avatarUrl: ""), body: "Abcdef",
                        createdAt: Date(), updatedAt: Date()),
            
            PullRequest(id: 3, url: "http://github.com/a/b/pulls/3", title: "Pull request 3",
                        user: Author(name: "User 3", avatarUrl: ""), body: "Abcdef",
                        createdAt: Date(), updatedAt: Date())
        ]
        
        tableView = UITableView()
        sut = PullRequestsDataSource(pullRequests: pullRequests, tableView: tableView)
    }
    
    func testInit() {
        XCTAssertNotNil(sut)
    }
    
    func testDataSourceSetProperRowHeight() {
        XCTAssertEqual(tableView.rowHeight, UITableViewAutomaticDimension)
        XCTAssertEqual(tableView.estimatedRowHeight, 113)
    }
    
    func testDataSourceSetTableViewDataSourceDelegate() {
        XCTAssert(tableView.dataSource === sut)
    }
    
    func testDataSourceRegistersCellNibProperly() {
        // Use raw identifiers here
        tableView.dequeueReusableCell(withIdentifier: "\(PullRequestTableViewCell.self)",
            for: IndexPath(row: 0, section: 0))
    }
    
    func testDataSourceReportsCorrectNumberOfRows() {
        XCTAssertEqual(sut.tableView(tableView, numberOfRowsInSection: 0), pullRequests.count)
    }
    
    func testDequeueProperCellTypeConfigured() {
        let cell = sut.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 0))
        
        XCTAssert(cell is PullRequestTableViewCell)
        
        XCTAssertEqual(cell.accessoryType, .disclosureIndicator)
    }
}
