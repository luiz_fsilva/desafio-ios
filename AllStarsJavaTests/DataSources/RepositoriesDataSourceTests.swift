//
//  RepositoriesDataSourceTests.swift
//  AllStarsJavaTests
//
//  Created by Luiz Fernando Silva on 02/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import XCTest
@testable import AllStarsJava

class RepositoriesDataSourceTests: XCTestCase {
    
    var sut: RepositoriesDataSource!
    
    fileprivate var tableView: UITableView!
    var repositories: [Repository] = []
    
    override func setUp() {
        super.setUp()
        
        repositories = [
            Repository(name: "Repo 1", description: "Repo 1",
                       author: Author(name: "gitguy1", avatarUrl: "https://www.example.com/img-1.jpg"),
                       starsCount: 0, forksCount: 0),
            
            Repository(name: "Repo 2", description: "Repo 2",
                       author: Author(name: "gitguy2", avatarUrl: "https://www.example.com/img-2.jpg"),
                       starsCount: 1, forksCount: 1),
            
            Repository(name: "Repo 3", description: "Repo 3",
                       author: Author(name: "gitguy3", avatarUrl: "https://www.example.com/img-3.jpg"),
                       starsCount: 2, forksCount: 2)
        ]
        
        tableView = UITableView()
        sut = RepositoriesDataSource(repositories: repositories, tableView: tableView)
    }
    
    func testInit() {
        XCTAssertNotNil(sut)
    }
    
    func testDataSourceSetProperRowHeight() {
        XCTAssertEqual(tableView.rowHeight, UITableViewAutomaticDimension)
        XCTAssertEqual(tableView.estimatedRowHeight, 72)
    }
    
    func testDataSourceSetTableViewDataSourceDelegate() {
        XCTAssert(tableView.dataSource === sut)
    }
    
    func testDataSourceRegistersCellNibProperly() {
        // Use raw identifiers here
        tableView.dequeueReusableCell(withIdentifier: "\(RepositoryTableViewCell.self)",
                                      for: IndexPath(row: 0, section: 0))
    }
    
    func testDataSourceReportsCorrectNumberOfRows() {
        XCTAssertEqual(sut.tableView(tableView, numberOfRowsInSection: 0), repositories.count)
    }
    
    func testDequeueProperCellTypeConfigured() {
        let cell = sut.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 0))
        
        XCTAssert(cell is RepositoryTableViewCell)
        
        XCTAssertEqual(cell.accessoryType, .disclosureIndicator)
    }
}
