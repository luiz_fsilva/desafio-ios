//
//  LoadMoreTableViewDelegateTests.swift
//  AllStarsJavaTests
//
//  Created by Luiz Fernando Silva on 02/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import XCTest
@testable import AllStarsJava

class LoadMoreTableViewDelegateTests: XCTestCase {
    
    var tableView: UITableView!
    var didFireCallback = false
    
    var sut: LoadMoreTableViewDelegate!
    
    override func setUp() {
        super.setUp()
        
        didFireCallback = false
        tableView = UITableView()
        
        sut = LoadMoreTableViewDelegate(tableView: tableView, callback: { [weak self] in
            self?.didFireCallback = true
        })
    }
    
    func testDelegateSetTableViewsDelegateToSelf() {
        XCTAssert(tableView.delegate === sut)
    }
    
    func testDelegateFiresWhenDraggingBeyondBottom() {
        // Note: We feed a scroll view here just for testing, but a table view
        // will work just as fine (since it's a subclass of UIScrollView itself)
        
        let scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: 100, height: 200))
        scrollView.contentSize = CGSize(width: 100, height: 400)
        scrollView.contentOffset = CGPoint(x: 0, y: 210)
        
        sut.distance = 10
        
        var target = CGPoint(x: 0, y: 210)
        sut.scrollViewWillEndDragging(scrollView, withVelocity: CGPoint(x: 0, y: 0), targetContentOffset: &target)
        
        XCTAssertTrue(didFireCallback)
    }
    
    func testDelegateDoesNotFireWhenDraggingWithoutExceedingBottomOfScrollView() {
        let scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: 100, height: 200))
        scrollView.contentSize = CGSize(width: 100, height: 400)
        scrollView.contentOffset = CGPoint(x: 0, y: 209)
        
        sut.distance = 10
        
        var target = CGPoint(x: 0, y: 200)
        sut.scrollViewWillEndDragging(scrollView, withVelocity: CGPoint(x: 0, y: 0), targetContentOffset: &target)
        
        XCTAssertFalse(didFireCallback)
    }
    
    func testCallsDidSelectCell() {
        var called = false
        
        sut.didSelectCell = { index in
            XCTAssertEqual(index, IndexPath(row: 1, section: 2))
            called = true
        }
        
        sut.tableView(tableView, didSelectRowAt: IndexPath(row: 1, section: 2))
        
        XCTAssert(called)
    }
}
