//
//  PullRequestsViewControllerTests.swift
//  AllStarsJavaTests
//
//  Created by Luiz Fernando Silva on 03/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import XCTest
@testable import AllStarsJava

class PullRequestsViewControllerTests: XCTestCase {
    
    var sut: PullRequestsViewController!
    
    var repository: Repository!
    var pullRequests: [PullRequest] = []
    
    override func setUp() {
        super.setUp()
        
        pullRequests = [
            PullRequest(id: 1, url: "https://github.com/a/b/pulls/1", title: "Pull request 1",
                        user: Author(name: "User 1", avatarUrl: ""), body: "Abcdef",
                        createdAt: Date(), updatedAt: Date()),
            
            PullRequest(id: 2, url: "http://github.com/a/b/pulls/2", title: "Pull request 2",
                        user: Author(name: "User 2", avatarUrl: ""), body: "Abcdef",
                        createdAt: Date(), updatedAt: Date()),
            
            PullRequest(id: 3, url: "http://github.com/a/b/pulls/3", title: "Pull request 3",
                        user: Author(name: "User 3", avatarUrl: ""), body: "Abcdef",
                        createdAt: Date(), updatedAt: Date())
        ]
        
        repository =
            Repository(name: "Repo 1", description: "Repo 1",
                       author: Author(name: "gitguy1", avatarUrl: ""),
                       starsCount: 0, forksCount: 0)
        
        sut =
            UIStoryboard(name: "Main", bundle: Bundle(for: PullRequestsViewController.self))
                .instantiateViewController(withIdentifier: "pullRequests")
            as? PullRequestsViewController
        
        sut.repository = repository
        
        _=sut.view // Force-load view
        
        sut.dataSource.pullRequests = pullRequests
    }
    
    func testInit() {
        XCTAssertNotNil(sut)
    }
    
    func testSetRepositoryNameAsTitleOnViewDidLoad() {
        XCTAssertEqual(sut.title, repository.name)
    }
    
    func testCallsSetupTitleOnViewDidLoad() {
        let mock = MockPullRequestsViewController()
        
        _=mock.view // Force-load view to call viewDidLoad
        
        XCTAssert(mock._calledSetupTitle)
    }
    
    func testCallsSetupDataSourceOnViewDidLoad() {
        let mock = MockPullRequestsViewController()
        
        _=mock.view // Force-load view to call viewDidLoad
        
        XCTAssert(mock._calledSetupDataSource)
    }
    
    func testCallsSetupLoadMoreDelegateOnViewDidLoad() {
        let mock = MockPullRequestsViewController()
        
        _=mock.view // Force-load view to call viewDidLoad
        
        XCTAssert(mock._calledSetupLoadMoreDelegate)
    }
}

class MockPullRequestsViewController: PullRequestsViewController {
    
    var _calledSetupTitle = false
    var _calledSetupDataSource = false
    var _calledSetupLoadMoreDelegate = false
    
    override func setupTitle() {
        _calledSetupTitle = true
    }
    
    override func setupDataSource() {
        _calledSetupDataSource = true
    }
    
    override func setupLoadMoreDelegate() {
        _calledSetupLoadMoreDelegate = true
    }
}
