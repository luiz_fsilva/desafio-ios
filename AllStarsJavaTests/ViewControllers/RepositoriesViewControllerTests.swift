//
//  RepositoriesViewControllerTests.swift
//  AllStarsJavaTests
//
//  Created by Luiz Fernando Silva on 03/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import XCTest
@testable import AllStarsJava

class RepositoriesViewControllerTests: XCTestCase {
    
    var sut: RepositoriesViewController!
    
    var repositories: [Repository] = []
    
    override func setUp() {
        super.setUp()
        
        sut =
            UIStoryboard(name: "Main", bundle: Bundle(for: RepositoriesViewController.self))
                .instantiateViewController(withIdentifier: "repositories")
                    as? RepositoriesViewController
        
        _=sut.view // Force-load view
        
        repositories = [
            Repository(name: "Repo 1", description: "Repo 1",
                       author: Author(name: "gitguy1", avatarUrl: "https://www.example.com/img-1.jpg"),
                       starsCount: 0, forksCount: 0),
            
            Repository(name: "Repo 2", description: "Repo 2",
                       author: Author(name: "gitguy2", avatarUrl: "https://www.example.com/img-2.jpg"),
                       starsCount: 1, forksCount: 1),
            
            Repository(name: "Repo 3", description: "Repo 3",
                       author: Author(name: "gitguy3", avatarUrl: "https://www.example.com/img-3.jpg"),
                       starsCount: 2, forksCount: 2)
        ]
        
        sut.dataSource.repositories = repositories
    }
    
    func testInit() {
        XCTAssertNotNil(sut)
    }
    
    func testSegueIdentifier() {
        XCTAssertEqual(sut.pullRequestsSegue, "pullRequests")
    }
    
    func testCallsSetupDataSourceOnViewDidLoad() {
        let mock = MockRepositoriesViewController()
        
        _=mock.view // Force-load view to call viewDidLoad
        
        XCTAssert(mock._calledSetupDataSource)
    }
    
    func testCallsSetupLoadMoreDelegateOnViewDidLoad() {
        let mock = MockRepositoriesViewController()
        
        _=mock.view // Force-load view to call viewDidLoad
        
        XCTAssert(mock._calledSetupLoadMoreDelegate)
    }
}

class MockRepositoriesViewController: RepositoriesViewController {
    
    var _calledSetupDataSource = false
    var _calledSetupLoadMoreDelegate = false
    
    override func setupDataSource() {
        _calledSetupDataSource = true
    }
    
    override func setupLoadMoreDelegate() {
        _calledSetupLoadMoreDelegate = true
    }
}
