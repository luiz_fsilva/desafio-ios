//
//  PullRequestTableViewCellTests.swift
//  AllStarsJavaTests
//
//  Created by Luiz Fernando Silva on 03/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import XCTest
@testable import AllStarsJava

class PullRequestTableViewCellTests: XCTestCase {
    
    var mockAvatarDataSource: MockAuthorAvatarDataSource!
    var sut: PullRequestTableViewCell!
    
    override func setUp() {
        super.setUp()
        
        mockAvatarDataSource = MockAuthorAvatarDataSource()
        
        sut = PullRequestTableViewCell.instantiate()
        sut.authorAvatarDataSource = mockAvatarDataSource
    }
    
    func testInit() {
        XCTAssertNotNil(sut)
    }
    
    func testConfigureCell() {
        let pull =
            PullRequest(id: 1, url: "https://github.com/a/b/pulls/1", title: "Pull request 1",
                        user: Author(name: "User 1", avatarUrl: ""),
                        body: "Pull request body 1", createdAt: Date(), updatedAt: Date())
        
        sut.configure(with: pull)
        
        XCTAssertEqual(sut.lblTitle.text, pull.title)
        XCTAssertEqual(sut.lblBody.text, pull.body)
        XCTAssertEqual(sut.lblAuthorName.text, pull.user.name)
    }
    
    func testBodyIsParsedWithMarkdown() {
        let mock = MockMarkdown()
        
        let pull =
            PullRequest(id: 1, url: "", title: "", user: Author(name: "User 1", avatarUrl: ""),
                        body: "Pull request body 1", createdAt: Date(), updatedAt: Date())
        
        sut.markdown = mock
        
        sut.configure(with: pull)
        
        XCTAssertEqual(mock._pullRequest?.title, pull.title)
        XCTAssertEqual(mock._pullRequest?.body, pull.body)
    }
    
    func testCalledDidTryLoadingAuthorAvatarImage() {
        let author = Author(name: "Owner", avatarUrl: "http://www.example.com/url")
        let pull =
            PullRequest(id: 1, url: "https://github.com/a/b/pulls/1", title: "Pull request 1",
                        user: author, body: "Pull request body 1", createdAt: Date(),
                        updatedAt: Date())
        
        sut.configure(with: pull)
        
        XCTAssertEqual(mockAvatarDataSource._author?.name, author.name)
        XCTAssertEqual(mockAvatarDataSource._author?.avatarUrl, author.avatarUrl)
        
        XCTAssertEqual(mockAvatarDataSource._imageView, sut.imgUserAvatar)
    }
    
    func testCancelsPreviousAvatarDownloadOnConfigure() {
        let author = Author(name: "Owner", avatarUrl: "http://www.example.com/url")
        let pull =
            PullRequest(id: 3, url: "https://github.com/a/b/pulls/1", title: "Pull request 1",
                        user: author, body: "Pull request body 1", createdAt: Date(),
                        updatedAt: Date())
        
        sut.configure(with: pull)
        
        let lastCancellable = mockAvatarDataSource._cancellable!
        XCTAssertEqual(lastCancellable.wasCancelled, false)
        
        sut.configure(with: pull)
        
        XCTAssertEqual(lastCancellable.wasCancelled, true)
    }
}

class MockMarkdown: Markdown {
    
    var _pullRequest: PullRequest?
    
    func markdownBody(for pullRequest: PullRequest) -> NSAttributedString {
        _pullRequest = pullRequest
        
        return NSAttributedString(string: "")
    }
}
