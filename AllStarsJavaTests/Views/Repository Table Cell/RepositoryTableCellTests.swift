//
//  RepositoryTableCellTests.swift
//  AllStarsJavaTests
//
//  Created by Luiz Fernando Silva on 02/11/17.
//  Copyright © 2017 Luiz Fernando Silva. All rights reserved.
//

import XCTest
@testable import AllStarsJava

class RepositoryTableCellTests: XCTestCase {
    
    var mockAvatarDataSource: MockAuthorAvatarDataSource!
    var sut: RepositoryTableViewCell!
    
    override func setUp() {
        super.setUp()
        
        mockAvatarDataSource = MockAuthorAvatarDataSource()
        sut = RepositoryTableViewCell.instantiate()
        
        sut.authorAvatarDataSource = mockAvatarDataSource
    }
    
    func testInit() {
        XCTAssertNotNil(sut)
    }
    
    func testConfigureCell() {
        let repo = Repository(name: "Repo 1", description: "Test",
                              author: Author(name: "Owner", avatarUrl: ""),
                              starsCount: 3, forksCount: 4000)
        
        sut.configure(with: repo)
        
        XCTAssertEqual(sut.lblName.text, "\(repo.author.name)/\(repo.name)")
        
        XCTAssertEqual(sut.lblStarCount.text, "3")
        XCTAssertEqual(sut.lblForkCount.text, "4,000")
    }
    
    func testCalledDidTryLoadingAuthorAvatarImage() {
        let author = Author(name: "Owner", avatarUrl: "http://www.example.com/url")
        let repo = Repository(name: "Repo 1", description: "Test",
                              author: author, starsCount: 3, forksCount: 4000)
        
        sut.configure(with: repo)
        
        XCTAssertEqual(mockAvatarDataSource._author?.name, author.name)
        XCTAssertEqual(mockAvatarDataSource._author?.avatarUrl, author.avatarUrl)
        
        XCTAssertEqual(mockAvatarDataSource._imageView, sut.imgUserAvatar)
    }
    
    func testCancelsPreviousAvatarDownloadOnConfigure() {
        let author = Author(name: "Owner", avatarUrl: "http://www.example.com/url")
        let repo = Repository(name: "Repo 1", description: "Test",
                              author: author, starsCount: 3, forksCount: 4000)
        
        sut.configure(with: repo)
        
        let lastCancellable = mockAvatarDataSource._cancellable!
        XCTAssertEqual(lastCancellable.wasCancelled, false)
        
        sut.configure(with: repo)
        
        XCTAssertEqual(lastCancellable.wasCancelled, true)
    }
}

class MockAuthorAvatarDataSource: AuthorAvatarDataSource {
    var _author: Author?
    var _imageView: UIImageView?
    var _placeholder: UIImage?
    var _cancellable: MockCancellable?
    
    func loadAuthorAvatar(_ author: Author, to imageView: UIImageView, placeholder: UIImage?) -> Cancellable? {
        _author = author
        _imageView = imageView
        _placeholder = placeholder
        _cancellable = MockCancellable()
        
        return _cancellable
    }
    
    class MockCancellable: Cancellable {
        var wasCancelled = false
        
        func cancel() {
            wasCancelled = true
        }
    }
}
